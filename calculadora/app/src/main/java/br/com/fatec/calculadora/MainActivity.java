package br.com.fatec.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    float ValorUm, ValorDois;
    boolean adicao, subtracao, multiplicao, divisao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);

    }



    public void b1(View v) {

        textView.setText(textView.getText() + "1");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();

    }

    public void b2(View v) {

        textView.setText(textView.getText() + "2");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b3(View v) {

        textView.setText(textView.getText() + "3");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b4(View v) {

        textView.setText(textView.getText() + "4");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b5(View v) {

        textView.setText(textView.getText() + "5");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b6(View v) {

        textView.setText(textView.getText() + "6");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b7(View v) {

        textView.setText(textView.getText() + "7");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b8(View v) {

        textView.setText(textView.getText() + "8");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b9(View v) {

        textView.setText(textView.getText() + "9");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void b0(View v) {

        textView.setText(textView.getText() + "0");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void bDot(View v) {

        textView.setText(textView.getText() + ".");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void limpa(View v) {

        textView.setText("");
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }


    public void add(View v) {

        if (textView == null) {
            textView.setText("");
        } else {
            ValorUm = Float.parseFloat(textView.getText() + "");
            adicao = true;
            textView.setText(null);
        }
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void multi(View v) {

        if (textView == null) {
            textView.setText("");
        } else {
            ValorUm = Float.parseFloat(textView.getText() + "");
            multiplicao = true;
            textView.setText(null);
        }
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }


    public void div(View v) {
        if (textView == null) {
            textView.setText("");
        } else {
            ValorUm = Float.parseFloat(textView.getText() + "");
            divisao = true;
            textView.setText(null);
        }
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void sub(View v) {
        if (textView == null) {
            textView.setText("");
        } else {
            ValorUm = Float.parseFloat(textView.getText() + "");
            subtracao = true;
            textView.setText(null);
        }
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }

    public void igual(View v) {
        ValorDois = Float.parseFloat(textView.getText() + "");

        if (adicao == true) {
            textView.setText(ValorUm + ValorDois + "");
            adicao = false;
        }

        if (subtracao == true) {
            textView.setText(ValorUm - ValorDois + "");
            subtracao = false;
        }

        if (multiplicao == true) {
            textView.setText(ValorUm * ValorDois + "");
            multiplicao = false;
        }

        if (divisao == true) {
            textView.setText(ValorUm / ValorDois + "");
            divisao = false;
        }
        MediaPlayer som = MediaPlayer.create(this, R.raw.click);
        som.start();
    }


}




