package br.com.fatec.jogo_da_velha;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button bt11, bt12, bt13, bt21, bt22, bt23, bt31, bt32, bt33; // variaveis botão
    String jogador = "X";
    int qtdejogadas = 0;
    int contx = 0;
    int conto = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt11 = (Button) findViewById(R.id.btn11); //Instanciando
        bt12 = (Button) findViewById(R.id.btn12);
        bt13 = (Button) findViewById(R.id.btn13);

        bt21 = (Button) findViewById(R.id.btn21);
        bt22 = (Button) findViewById(R.id.btn22);
        bt23 = (Button) findViewById(R.id.btn23);

        bt31 = (Button) findViewById(R.id.btn31);
        bt32 = (Button) findViewById(R.id.btn32);
        bt33 = (Button) findViewById(R.id.btn33);

        novo(); // chamando método novo
    }

    public void novo(){

       bt11.setText(""); //Limpar texto botoes
       bt12.setText("");
       bt13.setText("");

       bt21.setText("");
       bt22.setText("");
       bt23.setText("");

       bt31.setText("");
       bt32.setText("");
       bt33.setText("");

    }

    public void jogada(View v){ // onClick de todos os botões
        //colocar o arquivo mp3 na res > raw
        //MediaPlayer som = MediaPlayer.create(this, R.raw.nomedoarquivo);
        //som.start();  - quando eu chamar o jogada vai executar o som 1x


        Button jogada = (Button) findViewById(v.getId()); // get no botão selecionado


        if(jogada.getText().toString() == "") {
            if (jogador == "X") {
                jogada.setText("X");
                jogador = "O"; //troca jogador
                qtdejogadas++;

            } else {
                jogada.setText("O");
                jogador = "X";
                qtdejogadas++;
            }

            if(qtdejogadas == 9){
               if(contx > conto){
                   Toast.makeText(getApplicationContext(),"Jogador X Ganhou",Toast.LENGTH_SHORT).show();
               }
               else if(conto > contx){
                   Toast.makeText(getApplicationContext(),"Jogador O Ganhou",Toast.LENGTH_SHORT).show();

               }
               else{
                   Toast.makeText(getApplicationContext(),"Empate",Toast.LENGTH_SHORT).show();

               }
                novo();
           }


        }

    }
}
